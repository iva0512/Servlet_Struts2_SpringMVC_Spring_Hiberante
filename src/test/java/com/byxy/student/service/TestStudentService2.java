package com.byxy.student.service;

import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.byxy.student.entity.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-mybatis.xml"})
public class TestStudentService2 {

	@Resource
	private StudentService studentService;
	
	@Test
	public void testList() {
		List<Student> stds = studentService.findAll();
		for(Student s:stds) {
			System.out.println(s.getName());
		}
	}
	@Test
	public void add() {
		Student std = new Student();
		std.setPid(UUID.randomUUID().toString());
		std.setName(UUID.randomUUID().toString().replaceAll("-", ""));
		studentService.addPerson(std);
	}
	@Test
	public void testPage() {
		List<Student> stds = studentService.find(1,4);
		for(Student s:stds) {
			System.out.println(s.getName());
		}
	}
	@Test
	public void testcountAll() {
		System.out.println(studentService.countAll());
	}
}
