package com.byxy.student.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.byxy.student.service.UploadFileService;

@WebServlet("/servlet/file/FileList")
public class DownPageServlet extends HttpServlet {

	private UploadFileService fileService;

	@Override
	public void init() throws ServletException {
		super.init();
		if (null == fileService) {
			ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
			fileService = (UploadFileService) ctx.getBean(UploadFileService.class);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("list", fileService.findFileList());
		req.getRequestDispatcher("/WEB-INF/jsp/person/downFile.jsp").forward(req, resp);
	}
}
