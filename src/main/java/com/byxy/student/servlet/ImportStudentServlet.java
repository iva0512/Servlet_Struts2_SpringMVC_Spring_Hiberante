package com.byxy.student.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.byxy.student.entity.Student;
import com.byxy.student.service.StudentService;

/** 读取xml，录入演示数据 */
@WebServlet("/servlet/std/ImportStudent")
public class ImportStudentServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StudentService studentService;

	// Servlet不支持Spring 的自动注入，需要手动
	@Override
	public void init() throws ServletException {

		super.init();
		if (null == studentService) {
			ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
			studentService = (StudentService) ctx.getBean(StudentService.class);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 解析studentData.xml文件
		// 创建SAXReader的对象reader
		SAXReader reader = new SAXReader();
		try {
			// 通过reader对象的read方法加载studentData.xml文件,获取docuemnt对象。
			Document document = reader
					.read(new File(this.getClass().getClassLoader().getResource("").getPath() + "/studentData.xml"));
			// 通过document对象获取根节点students
			Element students = document.getRootElement();
			// 通过element对象的elementIterator方法获取迭代器
			Iterator it = students.elementIterator();
			// 遍历迭代器，获取根节点中的信息（书籍）
			while (it.hasNext()) {
				Element student = (Element) it.next();
				// 获取student的id属性值
				String id = student.attribute("id").getValue();
				String cellphone = student.element("cellphone").getTextTrim();
				String name = student.element("name").getTextTrim();
				String password = student.element("password").getTextTrim();
				String address = student.element("address").getTextTrim();
				String dorm = student.element("dorm").getTextTrim();
				String remarks = student.element("remarks").getTextTrim();
				String job = student.element("job").getTextTrim();
				String relationship = student.element("relationship").getTextTrim();
				String relationshipPhone = student.element("relationshipPhone").getTextTrim();
				Student person = new Student(id, cellphone, name, password, address, dorm, remarks, job, relationship,
						relationshipPhone);
				studentService.addPerson(person);
			}
			req.getSession().setAttribute("tishi", "导入成功");
			resp.sendRedirect("../../index.jsp");
		} catch (DocumentException e) {
			e.printStackTrace();
			req.getSession().setAttribute("tishi", "导入失败");
			resp.sendRedirect("../../index.jsp");
		}
	}

}
