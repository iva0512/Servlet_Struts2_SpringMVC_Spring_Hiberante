package com.byxy.student.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.byxy.student.dao.UploadFileDao;
import com.byxy.student.entity.UploadFile;

@Repository
public class UploadFileDaoImpl implements UploadFileDao {

	@Resource
	private SessionFactory sessionFactory;

	@Override
	public void add(UploadFile file) {

		sessionFactory.getCurrentSession().save(file);
	}

	@Override
	public List<UploadFile> findAll() {
		return sessionFactory.getCurrentSession().createCriteria(UploadFile.class).list();
	}

	@Override
	public UploadFile findById(int id) {
		return (UploadFile) sessionFactory.getCurrentSession().get(UploadFile.class, id);
	}
}
