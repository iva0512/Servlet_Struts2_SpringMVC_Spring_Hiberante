package com.byxy.student.dao;

import com.byxy.student.entity.UploadFile;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public interface UploadFileDao {

    public void add(UploadFile file);

    public List<UploadFile> findAll();

    public UploadFile findById(int id);

}
