package com.byxy.student.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import com.byxy.student.entity.Student;
import com.byxy.student.entity.StudentLog;

@MapperScan
public interface StudentLogDao {

	/**
	 * 统计记录数
	 * 
	 * @return
	 */
	int count();

	/**
	 * 分页查询
	 * 
	 * @param curPage
	 *            当下标 （从0开孡的正数）
	 * @param pageCount
	 *            行数（从1开始的正数）
	 * @return
	 */
	List<StudentLog> find(@Param("pos") int pos, @Param("rows") int rows);

	/**
	 * 保存记录
	 * 
	 * @param p
	 */
	void add(StudentLog p);

}