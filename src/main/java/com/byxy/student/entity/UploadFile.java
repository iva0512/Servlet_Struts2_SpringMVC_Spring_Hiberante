package com.byxy.student.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "uploadfile")
public class UploadFile implements Serializable {

	@Id
	@GeneratedValue
	private Integer id;
	//
	private String name;
	//
	private String path;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {		
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
