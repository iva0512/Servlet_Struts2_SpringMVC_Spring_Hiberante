package com.byxy.student.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.byxy.student.dao.UploadFileDao;
import com.byxy.student.entity.UploadFile;
import com.byxy.student.service.UploadFileService;

@Service
@Transactional
public class UploadFileServiceImpl implements UploadFileService {

    @Resource
    private UploadFileDao fileDao ;

    @Override
    public void addFile(UploadFile file) {
        fileDao.add(file);
    }

    @Override
    public List<UploadFile> findFileList() {
        return fileDao.findAll();
    }

    @Override
    public UploadFile findFileById(int id) {
        return fileDao.findById(id);
    }


}
