package com.byxy.student.service;

import com.byxy.student.entity.UploadFile;

import java.util.List;

public interface UploadFileService {

	public void addFile(UploadFile file);

	public List<UploadFile> findFileList();

	public UploadFile findFileById(int id);

}
